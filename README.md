# QuickClique

*Tagline*

Solving problems on quantum hardware faster.

*Introduction & Problem*

Many near-term quantum computing applications, such as simulation of molecular behavior in industries such as pharma and chemicals utilize quantum hardware to measure hamiltonian operators. The hamiltonian operator can scale to an incredibly large number of terms, creating a burden on existing quantum hardware, and requiring an abundance of measurements. This creates limitations on the practicality of the Hamiltonian as a tool within quantum computing.

*Solution*

Based on https://arxiv.org/pdf/1907.03358.pdf, the QuickClique solver uses a MinClique algorithm to group Hamiltonian terms together and therefore reduce the total number of measurements required for a Hamiltonian-based computation.


*Submission*

To be discussed in detail the presentation, but a number of implementations have been made in each of Xanadu, Dwave, IBM, and Rigetti environments.


*Operating Instructions*

See each respective folder for sample implementations and demonstrations of the concept. The files may need to be included into a local installation of each respective environment to run.
For Xanadu - only thr additions to the GBS library have been uploaded - GBS has not yet been made public. To run the code, include the files in the GBS library (if access is provided by Xanadu)